import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class exercici1Test {

	 @Test
	    public void testSumaPositivos() {
	        Calculadora calc = new Calculadora();
	        assertEquals(10.0,calc.suma(5, 5));
	        assertEquals(12,calc.suma(6, 6));
	        assertEquals(15,calc.suma(7, 8));
	    }

	    @Test
	    public void testRestaPositivos() {
	        Calculadora calc = new Calculadora();
	        assertEquals(5.0, calc.resta(10, 5));
	        assertEquals(12, calc.resta(24, 12));

	    }

	 
	    @Test
	    public void testMultiplicacionPositivos() {
	        Calculadora calc = new Calculadora(); 
	        assertEquals(50.0, calc.multiplicacion(25, 2));
	        assertEquals(16, calc.multiplicacion(8, 2));
	        assertEquals(28, calc.multiplicacion(7, 4));


	    }

	    @Test
	    public void testDivisionPositivos() {
	        Calculadora calc = new Calculadora();	        
	        assertEquals(2.0, calc.division(10, 5));
	        assertEquals(7, calc.division(35, 5));

	    }
}
