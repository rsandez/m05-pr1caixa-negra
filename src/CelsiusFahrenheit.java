import java.util.Scanner;

public class CelsiusFahrenheit {
	
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingrese la temperatura en grados Celsius: ");
        double celsius = input.nextDouble();

        double fahrenheit = celsiusToFahrenheit(celsius, 1.8, 32);

        System.out.println(celsius + " grados Celsius equivale a " + fahrenheit + " grados Fahrenheit.");
    }

    public static double celsiusToFahrenheit(double celsius, double factor, double adjustment) {
        double fahrenheit = celsius * factor + adjustment;
        return fahrenheit;
    }
}
