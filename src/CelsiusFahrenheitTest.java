import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CelsiusFahrenheitTest {

	@Test
	public void testCelsius() {
		CelsiusFahrenheit celfah = new CelsiusFahrenheit();
		
		assertEquals(50, celfah.celsiusToFahrenheit(10, 1.8, 32));
		assertEquals(75.2, celfah.celsiusToFahrenheit(24, 1.8, 32));
		assertEquals(89.6, celfah.celsiusToFahrenheit(32, 1.8, 32));
		assertEquals(109.4, celfah.celsiusToFahrenheit(43, 1.8, 32));
		assertEquals(39.2, celfah.celsiusToFahrenheit(4, 1.8, 32));
		assertEquals(42.8, celfah.celsiusToFahrenheit(6, 1.8, 32));
		assertEquals(46.4, celfah.celsiusToFahrenheit(8, 1.8, 32));
		assertEquals(122, celfah.celsiusToFahrenheit(50, 1.8, 32));
		assertEquals(62.6, celfah.celsiusToFahrenheit(17, 1.8, 32));
		assertEquals(73.4, celfah.celsiusToFahrenheit(23, 1.8, 32));

	}

}
