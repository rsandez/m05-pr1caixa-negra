import java.util.Scanner;

		public class Calculadora {
			
			 public static void main(String[] args) {
				 
				 Scanner input = new Scanner(System.in);

			        double num1;
			        double num2;
			        char operador;

			        System.out.print("Ingrese el primer número: ");
			        num1 = input.nextDouble();

			        System.out.print("Ingrese el segundo número: ");
			        num2 = input.nextDouble();

			        System.out.print("Ingrese el operador (+, -, *, /): ");
			        operador = input.next().charAt(0);

			        double resultado = 0;

			        switch(operador) {
			            case '+':
			                resultado = suma(num1, num2);
			                break;
			            case '-':
			                resultado = resta(num1, num2);
			                break;
			            case '*':
			                resultado = multiplicacion(num1, num2);
			                break;
			            case '/':
			                if(num2 == 0) {
			                    System.out.println("No se puede dividir entre cero.");
			                    return;
			                }
			                resultado = division(num1, num2);
			                break;
			            default:
			                System.out.println("Operador no válido.");
			                return;
			        }

			        System.out.println(num1 + " " + operador + " " + num2 + " = " + resultado);
			    }
				 

		    public static double suma(double num1, double num2) {
		        return num1 + num2;
		    }

		    public static double resta(double num1, double num2) {
		        return num1 - num2;
		    }

		    public static double multiplicacion(double num1, double num2) {
		        return num1 * num2;
		    }

		    public static double division(double num1, double num2) {
		        if(num2 == 0) {
		        	System.out.println("No se puede dividir entre cero.");
		        }
		        return num1 / num2;
		    }
		}
